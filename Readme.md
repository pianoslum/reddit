The script requieres [rtv](https://github.com/michael-lazar/rtv) to be installed.
After starting `reddit` for the first time you can add your favourite subreddits to `~/.reddit/subreddits`.
If the script is started without parameters, it chooses the subreddit you visited last;
if a subreddit name is passed, it will choose the top posts since your last visit.
